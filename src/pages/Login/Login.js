import React from 'react';
import Header from '../../components/Header';
import Footer from '../../components/Footer';
import LoginForm from "./loginForm";
import {connect} from "react-redux";
import { loginUser } from '../../store/actions/auth';

export class Login extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            selected : 1,
            errors:''
        };
    
        this.onLoginType = this.onLoginType.bind(this);
        this.submit = this.submit.bind(this);
    }

    componentDidMount() {
        if (this.props.user.isAuthenticated) {
            this.props.history.push('/dashboard');
        }
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        if (nextProps.user && nextProps.user.isAuthenticated) {
            this.props.history.push('/dashboard');
        }
        if (nextProps.errors) {
            this.setState({
                errors: nextProps.errors
            });
        }
    }

    submit(values) {
        values.loginType = this.state.selected;
        this.props.loginUser(values, this.props.history);
    }

    onLoginType(e){
        this.setState({
            selected: parseInt(e.currentTarget.value)
        });
    }

    render() {
        return (
            <React.Fragment>
                <Header/>
                <section className="login-section">
                    <div className="container">
                        <div className="login-form-outer">
                            <h1>Login as</h1>
                            <div className="login-cards-outer">
                                <input type="radio" id="admin" name="login_type" checked={this.state.selected === 1} onChange={this.onLoginType} value="1" className="d-none"/>
                                <label className="login-cards" htmlFor="admin">
                                    <span className="card-icon">
                                        <i className="icon-building"></i>
                                    </span>
                                    <span className="card-head">
                                        Admin
                                    </span>
                                </label>
                                <input type="radio" id="SalesPerson" name="login_type" onChange={this.onLoginType} checked={this.state.selected === 2} value="2" className="d-none"/>
                                <label className="login-cards" htmlFor="SalesPerson">
                                    <span className="card-icon">
                                        <i className="icon-user"></i>
                                    </span>
                                    <span className="card-head">
                                        Sales Person
                                    </span>
                                </label>
                            </div>
                            <h4>Let's get started as a { this.state.selected === 1 ? 'Admin' : 'Sales Person'} </h4>
                            <LoginForm errors={this.state.errors} onSubmit={this.submit}/>
                            <span className="switch-to-register">Don't have an account? <a href="/">Register</a></span>
                        </div>
                    </div>
                </section>
                <Footer/>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    user: state.user,
    errors: state.errors
});
export default connect(mapStateToProps, { loginUser })(Login)

