import React from 'react';
import {Field, reduxForm} from 'redux-form';

const validate = values => {
    const errors = {}
    if (!values.username) {
        errors.username = 'Username is Required'
    } else if (values.username.length < 2) {
        errors.username = 'Minimum be 2 characters or more'
    }
    // if (!values.email) {
    //     errors.email = 'Required'
    // } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    //     errors.email = 'Invalid email address'
    // }
    /*if (!val.age) {
        errors.age = 'Required'
    } else if (isNaN(Number(val.age))) {
        errors.age = 'Must be a number'
    } else if (Number(val.age) < 18) {
        errors.age = 'Sorry, you must be at least 18 years old'
    }*/
    if (!values.password) {
        errors.password = 'Password is Required'
    } else if (values.password.length < 2) {
        errors.password = 'Minimum be 2 characters or more'
    }
    return errors
}

const renderField = ({input, label, type, meta: {touched, error, warning}}) => (
    
    <div>
        <label className="control-label">{label}</label>
        <div>
            <input {...input} placeholder={label} type={type} className="form-control"/>
            {touched && ((error && <span className="text-danger">{error}</span>) || (warning && <span>{warning}</span>))}
        </div>
    </div>
)

let LoginForm = props => {
    const {handleSubmit, errors} = props;    
    return (
        <form onSubmit={handleSubmit} className="login-form">
            <div className="form-group">
                <Field name="username" component={renderField} label="Enter username"/>
            </div>
            <div className="form-group">
                <Field name="password" component={renderField} type="password" label="Enter Password"/>
            </div>
            <div className="form-check">
                <Field
                    name="remember"
                    id="remember"
                    component="input"
                    type="checkbox"
                    className="form-check-input"
                />
                <label className="form-check-label" htmlFor="remember">Remember Me</label>
                <label className="form-check-label float-right"><a href="/">Forgot Password?</a></label>
            </div>
            {errors && errors.loginError ? <div className="alert alert-danger" role="alert">{errors.loginError}</div> : ''}
            <button type="submit" className="btn btn-primary">Login</button>
        </form>
    )
}

LoginForm = reduxForm({
    form: 'login',
    validate,
})(LoginForm);
export default LoginForm;
