import React, { Component } from 'react';
import Header from '../../components/Header';
import Footer from '../../components/Footer';
import Card from '../../components/Card'
import { connect } from 'react-redux';
import {fetchDashboardCards} from '../../store/actions/fetchDashboardCards';

export class Dashboard extends Component {
    
    componentWillMount() {
        const {fetchDashboardCards} = this.props;
        fetchDashboardCards();
    }
    
    render() {
        const {cards} = this.props;
        return (
            <React.Fragment>
                <Header/>
                <section className="welcome-section">
                    <div className="container">
                        <div className="section-head">
                            <h4 className="text-left">Welcome Mayank,</h4>
                            <h1 className="text-left">Select industry to use on your project</h1>
                        </div>
                        <div className="row">
                            {
                                cards.map((card) =>
                                    <Card key={card.id} card={card}/>
                                )
                            }
                        </div>
                    </div>
                </section>
                <Footer/>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    cards: state.dashboard.cards,
    error: state.dashboard.error,
})

export default connect(
    mapStateToProps,
    { fetchDashboardCards }
)(Dashboard );

