import React, { Component } from 'react';
import Header from '../../components/Header';
import Footer from '../../components/Footer';
import Card from '../../components/Card'
import { connect } from 'react-redux';
import {fetchDashboardCards} from '../../store/actions/fetchDashboardCards';

export class Question extends Component {
    
    componentWillMount() {
        const {fetchDashboardCards} = this.props;
        fetchDashboardCards();
    }
    
    render() {
        const {cards} = this.props;
        return (
            <React.Fragment>
                <Header/>
                <section className="questions-main">
                    <div className="container">
                        <div className="section-head">
                            <h4 className="text-left">You have selected</h4>
                            <div className="industry-head-main">
                                <h1 className="text-left selected-industry" id="industryOptions" role="button"  data-toggle="dropdown">Events & Ticketing</h1>
                                <div className="dropdown-menu industry-option-menu" aria-labelledby="industryOptions">
                                    <a className="dropdown-item" href="#">Healthcare & Telemedicine</a>
                                    <a className="dropdown-item" href="#">Education & eLearning</a>
                                    <a className="dropdown-item active" href="#">Event & Ticketing</a>
                                    <a className="dropdown-item" href="#">Social Networking</a>
                                    <a className="dropdown-item" href="#">Food & Restaurant</a>
                                    <a className="dropdown-item" href="#">Media & Entertainment</a>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-sm-12 col-md-3">
                                <div className="by-module-main">
                                    <h2>By Modules</h2>
                                    <ul className="nav by-modules-list">
                                        <li className="nav-item">
                                            <a className="nav-link active" data-toggle="tab" href="#onboarding">Onboarding</a>
                                        </li>
                                        <li className="nav-item">
                                            <a className="nav-link" data-toggle="tab" href="#userProfile">User Profile</a>
                                        </li>
                                        <li className="nav-item">
                                            <a className="nav-link" data-toggle="tab" href="#eventListing">Event Listing</a>
                                        </li>
                                        <li className="nav-item">
                                            <a className="nav-link" data-toggle="tab" href="#eventDetail">Event Detail</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div className="col-sm-12 col-md-9">
                                <div className="question-select-main">
                                    <div className="question-select-head">
                                        <h2>Questions (54)</h2>
                                        <h5>Add New Question</h5>
                                    </div>
                                    <div className="tab-content">
                                        <div className="tab-pane active question-list-main" id="onboarding">
                                            <ul className="questions-list">
                                                <li className="questions-list-outer add-question-box">
                                                    <input type="text" placeholder="Write your question here"/>
                                                    <button type="button" className="btn btn-primary">Add</button>
                                                </li>
                                                <li className="questions-list-outer">
                                                    <label for="1">
                                                        Which type of profile do you want to others users?
                                                        <input type="checkbox" id="1"/>
                                                        <span className="checkmark"></span>
                                                    </label>
                                                </li>
                                                <li className="questions-list-outer">
                                                    <label for="2">
                                                        Which type of profile do you want to others users?
                                                        <input type="checkbox" id="2"/>
                                                        <span className="checkmark"></span>
                                                    </label>
                                                </li>
                                                <li className="questions-list-outer">
                                                    <label for="3">
                                                        Which type of profile do you want to others users?
                                                        <input type="checkbox" id="3"/>
                                                        <span className="checkmark"></span>
                                                    </label>
                                                </li>
                                                <li className="questions-list-outer">
                                                    <label for="4">
                                                        Which type of profile do you want to others users?
                                                        <input type="checkbox" id="4"/>
                                                        <span className="checkmark"></span>
                                                    </label>
                                                </li>
                                                <li className="questions-list-outer">
                                                    <label for="5">
                                                        Which type of profile do you want to others users?
                                                        <input type="checkbox" id="5"/>
                                                        <span className="checkmark"></span>
                                                    </label>
                                                </li>
                                                <li className="questions-list-outer">
                                                    <label for="7">
                                                        Which type of profile do you want to others users?
                                                        <input type="checkbox" id="7"/>
                                                        <span className="checkmark"></span>
                                                    </label>
                                                </li>
                                                <li className="questions-list-outer">
                                                    <label for="6">
                                                        Which type of profile do you want to others users?
                                                        <input type="checkbox" id="6"/>
                                                        <span className="checkmark"></span>
                                                    </label>
                                                </li>
                                            </ul>
                                        </div>
                                        <div className="tab-pane fade" id="userProfile">
                                            <h4 className="text-left">User Profile</h4>
                                        </div>
                                        <div className="tab-pane fade" id="eventListing">
                                            <h4 className="text-left">Event Listing</h4>
                                        </div>
                                        <div className="tab-pane fade" id="eventDetail">
                                            <h4 className="text-left">Event Detail</h4>
                                        </div>
                                    </div>
                                </div>
                                <div className="question-select-btns text-right">
                                    <button type="button" className="btn btn-primary reset-all">Reset All</button>
                                    <button type="button" className="btn btn-primary">Save</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <Footer/>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    cards: state.dashboard.cards,
    error: state.dashboard.error,
})

export default connect(
    mapStateToProps,
    { fetchDashboardCards }
)(Question);

