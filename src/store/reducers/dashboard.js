import { FETCH_DASHBOARD_CARDS_SUCCESS, FETCH_DASHBOARD_CARDS_ERROR } from "../actions/dashboard";

const initialState = {
  cards: [],
  error: null
}

export default function dashboard(state = initialState, action) {
  switch(action.type) {
    case FETCH_DASHBOARD_CARDS_SUCCESS:
      console.log('action', action);
      return {
        ...state,
        cards: action.payload
      }
    case FETCH_DASHBOARD_CARDS_ERROR:
      return {
        ...state,
        error: action.error
      }
    default:
      return state;
  }
}