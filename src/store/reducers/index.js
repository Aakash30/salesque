import { combineReducers } from "redux";
import user from './user';
import dashboard from './dashboard';
import {reducer as formReducer} from 'redux-form';
import errors from './errors';

export default combineReducers({
  user,
  form: formReducer,
  errors,
  dashboard
})
