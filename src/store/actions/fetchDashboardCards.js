import {fetchDashboardCardsSuccess, fetchDashboardCardsError} from './dashboard';
import AuthService from "../../services/AuthService";
import DashboardService from "../../services/DashboardService";

/**
 * Get Dasboard Cards
 */
export const fetchDashboardCards = () => {
  return dispatch => {
    const token = AuthService.getToken();
    console.log('token: ', token);
    DashboardService.dashboardCards().then(resp => {
      console.log('resp', resp);
      if (resp.data.success) {
        dispatch(fetchDashboardCardsSuccess(resp.data.cards));
      }
    }).catch(error => {
      if (error.resp.data) {
        dispatch(fetchDashboardCardsError(error.resp.data));
      }else{
        dispatch(fetchDashboardCardsError('Server error!'));
      }
    });
  };
};

