export const FETCH_DASHBOARD_CARDS_SUCCESS = 'FETCH_DASHBOARD_CARDS_SUCCESS';
export const FETCH_DASHBOARD_CARDS_ERROR = 'FETCH_DASHBOARD_CARDS_ERROR';

export function fetchDashboardCardsSuccess(cards) {
  return {
      type: FETCH_DASHBOARD_CARDS_SUCCESS,
      payload: cards
  }
}

export function fetchDashboardCardsError(error) {
  return {
      type: FETCH_DASHBOARD_CARDS_ERROR,
      error: error
  }
}