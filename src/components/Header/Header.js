import React from 'react';
import logo from '../../assets/images/logo.svg';
import userImg from '../../assets/images/user.svg';
import { withRouter} from 'react-router-dom';

import {connect} from 'react-redux';
import { logout } from '../../store/actions/auth';

export class Header extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {}
    }
    
    logout = () => this.props.logout(this.props.history);

    render() {

        const { user } = this.props;
        return (
            <header>
                <nav className="navbar navbar-expand-lg">
                    <div className="row">
                        <div className="col-3">
                            <a className="navbar-brand" href="/">
                                <img src={logo} width="50px" alt="logo"/>
                            </a>
                        </div>                       
                        <div className="col-9 text-right align-self-center">
                            {user.isAuthenticated ?
                                <div className="user-login">
                                    <div className="user-info">
                                        <div className="user-name">
                                            <img src={userImg} alt="user"/>
                                            <span className="user-name">{user.profile.firstName} {user.profile.lastName}</span>
                                        </div>
                                        <div className="user-menu">  
                                            <a className="icon-arrow-down" href="/" id="userOptions" role="button"  data-toggle="dropdown">
                                            </a>
                                            <div className="dropdown-menu" aria-labelledby="userOptions">
                                            <a className="dropdown-item" href="/">Profile</a>
                                            <span className="dropdown-item" onClick={this.logout}>Logout</span>
                                            </div>
                                        </div>
                                        <div className="side-bar-menu">
                                            <span className="side-menu"><i className="icon-menu"></i></span>
                                        </div>
                                    </div>
                                </div>
                            :
                                <div className="header-btns login-btns">
                                    <button type="button" className="btn btn-primary mr-1 active">Login</button>
                                    <button type="button" className="btn btn-primary">Sign Up</button>
                                </div>
                            }
                        </div>
                    </div>
                </nav>
            </header>
        );
    }
}
const mapStateToProps = state => ({
    user: state.user
});
  
export default connect(mapStateToProps, { logout })(withRouter(Header));