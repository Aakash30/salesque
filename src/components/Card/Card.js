import React from 'react';

export class Card extends React.PureComponent {
    render() {
        const {card} = this.props;
        return(
            <div className="col-12 col-sm-6 col-md-4 col-lg-3">
                <div className="login-cards">
                    <a href="/">
                        <span className="card-icon"><i className={card.icon}></i></span>
                        <span className="card-head">{card.title}</span>
                        <span className="questions-no">{card.questions} Questions</span>
                    </a>
                </div>
            </div>
        );
    }
};

export default Card;