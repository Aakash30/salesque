import React from 'react';

export class Footer extends React.PureComponent {
    render() {
        return(
            <footer>
                <div className="container">
                    <div className="row">
                        <div className="col-12 col-md-5 align-self-center">
                            <span>&copy; Copyright 2019 | Engineer Babu</span>
                        </div>
                        <div className="col-12 col-md-7 align-self-center">
                            <div className="footer-links">
                                <ul>
                                    <li>Privacy Policy</li>
                                    <li>Terms & Conditions</li>
                                    <li className="social-share">
                                        <i className="icon-facebook-logo"></i>
                                        <i className="icon-twitter"></i>
                                        <i className="icon-google-plus"></i>
                                    </li>
                                </ul>
                            </div>   
                        </div>
                    </div>
                </div>
            </footer>
        );
    }
};

export default Footer;