
/**
 * Dashboard Service
 */
import axios from 'axios';
import { API_URL } from '../config';
import AuthService from "./AuthService";

const DashboardService = {
  dashboardCards: function() {
    return axios.get(API_URL + '/dashboard', { headers: AuthService.authHeader() });
  },
}

export default DashboardService
