import React, { Component } from 'react';
import Login from './pages/Login';
import Dashboard from './pages/Dashboard';
import Question from './pages/Question';
import {connect} from "react-redux";
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import PrivateRoute from './Route/PrivateRoute';
import { getProfile } from './store/actions/user';

class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isLoading: true
        }
    }
    
    componentWillMount() {
        this.props.getProfile().finally(() => {
            this.setState({ isLoading: false });
        });
    }
    
    render() {
        if (this.state && this.state.isLoading === false) {
            return (
                <div>
                    <Switch>
                        <Route path="/login" component={Login}/>
                        <PrivateRoute path="/" component={Dashboard} exact />
                        <PrivateRoute component={Dashboard} path="/dashboard" exact />
                        <PrivateRoute component={Question} path="/question/:id" exact />
                    </Switch>
                </div>
            );
        }else{
            return (
                <div>Loading..</div>
            )
        }
    }
}

export default connect(null, { getProfile })(App);
