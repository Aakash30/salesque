import React from 'react';
import ReactDOM from 'react-dom';
import './assets/css/icon.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'jquery';
import 'bootstrap/dist/js/bootstrap.min.js';
import './assets/css/style.css';
import configureStore from './store/index';
import {BrowserRouter} from 'react-router-dom';
import {Provider} from 'react-redux';

import App from './App';
import * as serviceWorker from './serviceWorker';

const initialState = {
    user: {
        isAuthenticated: false,
        profile: {}
    }
};

const store = configureStore(initialState);
  
ReactDOM.render(
    <Provider store={store} >
        <BrowserRouter>
            <App />
        </BrowserRouter>
    </Provider>,
    document.getElementById('root')
);

serviceWorker.unregister();
